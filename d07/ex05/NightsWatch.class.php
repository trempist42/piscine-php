<?php

Class NightsWatch implements IFighter
{
	private $warrior = array();

	function recruit($recruit)
	{
		$this->warrior[] = $recruit;
	}

	function fight()
	{
		foreach ($this->warrior as $warrior)
			if ($warrior instanceof IFighter)
				$warrior->fight();
	}
}

?>