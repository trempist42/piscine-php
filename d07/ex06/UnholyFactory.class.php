<?php
class UnholyFactory
{
	private $fighters = array();
	
	public function absorb($fighter)
	{
		if (is_subclass_of($fighter, 'Fighter'))
		{
			if (isset($this->fighters[$fighter->getName()]))
			{
				print("(Factory already absorbed a fighter of type " . $fighter->getName() . ")" . PHP_EOL);
			}
			else
			{
				print("(Factory absorbed a fighter of type " . $fighter->getName() . ")" . PHP_EOL);
				$this->fighters[$fighter->getName()] = $fighter;
			}
		}
		else
		{
			print("(Factory can't absorb this, it's not a fighter)" . PHP_EOL);
		}
	}

	public function fabricate($requested_fighter)
	{
		if (array_key_exists($requested_fighter, $this->fighters))
		{
			print("(Factory fabricates a fighter of type " . $requested_fighter . ")" . PHP_EOL);
			return (clone $this->fighters[$requested_fighter]);
		}
		print("(Factory hasn't absorbed any fighter of type " . $requested_fighter . ")" . PHP_EOL);
	}
}