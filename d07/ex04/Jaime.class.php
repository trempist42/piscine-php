<?php

Class Jaime extends Lannister
{
	function sleepWith($somebody)
	{
		if ($somebody instanceof Tyrion)
			print "Not even if I'm drunk!" . PHP_EOL;
		elseif ($somebody instanceof Sansa)
			print "Let's do this." . PHP_EOL;
		elseif ($somebody instanceof Cersei)
			print "With pleasure, but only in a tower in Winterfell, then." . PHP_EOL;
	}
}

?>