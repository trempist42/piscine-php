<?php
session_start();
$con = mysqli_connect("127.0.0.1", "root", "root", "my_online_shop");
if ($con === false)
	exit("Error: Couldn't connect to DB!".mysqli_connect_error());
mysqli_select_db($con, "my_online_shop");
mysqli_set_charset($con, "utf8");
$query = "SELECT `id`, `login`, `password`, `type` FROM `users` ORDER BY `id` ASC ";

$queryfire = mysqli_query($con, $query);

$num = mysqli_num_rows($queryfire);

if (isset($_SESSION['user']))
{
	echo '<script>window.location="account.php"</script>';
	exit();
}

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['submit']))
{
	while ($account = mysqli_fetch_array($queryfire))
	{
		if ($account['login'] === $_POST['username'])
			break;
	}
	if ($account['password'] === hash("whirlpool", $_POST['password']))
	{
		$_SESSION['user'] = $account['login'];
		echo "<script type='text/javascript'>alert(`You're successfully logged in!`);</script>";
		echo '<script>window.location="account.php"</script>';
		exit();
	}
	else
	{
		echo "<script type='text/javascript'>alert(`Wrong username or password!`);</script>";
		echo '<script>window.location="login.php"</script>';
		exit();
	}
}
?>

<html>
<head>
	<title>My online shop | Login</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="styles.css">
</head>
<body style="font-family: 'Futura', sans-serif;">
<div class="navbar">
	<a href="./">Home</a>
	<a class="active" href="#">My account</a>
	<a href="cart.php">Cart</a>
</div>
<h1 class="header">My online shop</h1>
<h2	class="header">Account</h2>
</div>

<div class="wrapper">
	<h2>Login</h2>
	<p>Please fill in your credentials to login.</p>
	<form action="login.php" method="post">
		<div class="form-group ">
			<label>Username:</label>
			<input type="text" name="username" class="form-control" value="">
			<span class="help-block"></span>
		</div>
		<div class="form-group ">
			<label>Password:</label>
			<input type="password" name="password" class="form-control">
			<span class="help-block"></span>
		</div>
		<div class="form-group">
			<input type="submit" name="submit" value="Login" style="margin-top: 1rem">
		</div>
		<p>Don't have an account? <a href="register.php">Sign up now</a>.</p>
	</form>
</div>


</body>
</html>
