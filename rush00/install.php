<?php
function restoreDatabaseTables($dbHost, $dbUsername, $dbPassword, $dbName, $filePath){
	// Connect & select the database
	$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

	// Temporary variable, used to store current query
	$templine = '';

	// Read in entire file
	$lines = file($filePath);

	$error = '';

	// Loop through each line
	foreach ($lines as $line){
		// Skip it if it's a comment
		if(substr($line, 0, 2) == '--' || $line == ''){
			continue;
		}

		// Add this line to the current segment
		$templine .= $line;

		// If it has a semicolon at the end, it's the end of the query
		if (substr(trim($line), -1, 1) == ';'){
			// Perform the query
			if(!$db->query($templine)){
				$error .= 'Error performing query "<b>' . $templine . '</b>": ' . $db->error . '<br /><br />';
			}

			// Reset temp variable to empty
			$templine = '';
		}
	}
	return !empty($error)?$error:true;
}
$servername = "127.0.0.1";
$username = "root";
$password = "root";
$db_name = "my_online_shop";
$filePath = "my_online_shop.sql";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "CREATE DATABASE IF NOT EXISTS $db_name";
if ($conn->query($sql) === TRUE) {
	echo "Database created successfully!\n";
} else {
	echo "Error creating database: " . $conn->error . "\n";
}
if (restoreDatabaseTables($servername, $username, $password, $db_name, $filePath))
	echo "Database restored successfully!\nSite is ready to work!\n";
else
	echo "Error restoring database: " . $conn->error . "\n";
$conn->close();
?> 