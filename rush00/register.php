<?php
session_start();
$con = mysqli_connect("127.0.0.1", "root", "root", "my_online_shop");
if ($con === false)
	exit("Error: Couldn't connect to DB!".mysqli_connect_error());
mysqli_select_db($con, "users");

if (isset($_SESSION['user']))
{
	echo '<script>window.location="account.php"</script>';
	exit();
}

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['confirm_password']))
{
	if ($_POST['username'] && $_POST['password'] && $_POST['confirm_password']) {
		if ($_POST['password'] !== $_POST['confirm_password'])
		{
			echo "<script type='text/javascript'>alert(`Please, confirm your password!`);</script>";
			echo '<script>window.location="register.php"</script>';
			exit();
		}
		$query = "SELECT `id`, `login`, `password`, `type` FROM `users` ORDER BY `id` ASC ";

		$queryfire = mysqli_query($con, $query);

		$num = mysqli_num_rows($queryfire);

		while ($account = mysqli_fetch_array($queryfire)) {
			if ($account['login'] === $_POST['username']) {
				echo "<script type='text/javascript'>alert(`There are already an account with this login. Please, try another username.`);</script>";
				echo '<script>window.location="register.php"</script>';
				exit();
			}
		}
		$username = $_POST['username'];
		$password = $_POST['password'];
		$password = hash("whirlpool", $password);
		$query = "INSERT INTO `users` (`id`, `login`, `password`, `type`) VALUES (NULL, '$username', '$password', 'user');";

		$queryfire = mysqli_query($con, $query);
		echo "<script type='text/javascript'>alert(`You're successfully signed up! Please log in now with your credentials.`);</script>";
		echo '<script>window.location="login.php"</script>';
	}
	else
	{
		echo "<script type='text/javascript'>alert(`Please complete ALL fields.`);</script>";
		echo '<script>window.location="register.php"</script>';
		exit();
	}
}

?>

<html>
<head>
	<title>My online shop | Register</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="styles.css">
</head>
<body style="font-family: 'Futura', sans-serif;">
<div class="navbar">
	<a href="./">Home</a>
	<a class="active" href="#">My account</a>
	<a href="cart.php">Cart</a>
</div>
<h1 class="header">My online shop</h1>
<h2	class="header">Account</h2>
</div>

<div class="wrapper">
	<h2>Sign Up</h2>
	<p>Please fill this form to create an account.</p>
	<form action="register.php" method="post">
		<div class="form-group ">
			<label>Username:</label>
			<input type="text" name="username" class="form-control" value="">
			<span class="help-block"></span>
		</div>
		<div class="form-group ">
			<label>Password:</label>
			<input type="password" name="password" class="form-control" value="">
			<span class="help-block"></span>
		</div>
		<div class="form-group ">
			<label>Confirm Password:</label>
			<input type="password" name="confirm_password" class="form-control" value="">
			<span class="help-block"></span>
		</div>
		<div class="form-group">
			<input type="submit" name="submit" value="Submit" style="margin-top: 1rem;">
		</div>
		<p>Already have an account? <a href="login.php">Login here</a>.</p>
	</form>
</div>


</body>
</html>
