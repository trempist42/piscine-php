<?php
session_start();
if (!isset($_SESSION['user']))
{
	echo '<script>window.location="login.php"</script>';
	exit();
}
$con = mysqli_connect("127.0.0.1", "root", "root", "my_online_shop");
if ($con === false)
	exit("Error: Couldn't connect to DB!".mysqli_connect_error());
mysqli_select_db($con, "users");
if (isset($_POST['logout']) && $_POST['logout'] === "Log Out")
{
	session_destroy();
	echo "<script type='text/javascript'>alert(`You're successfully logged out!`);</script>";
	echo '<script>window.location="login.php"</script>';
	exit();
}

$query = "SELECT `id`, `login`, `password`, `type` FROM `users` ORDER BY `id` ASC ";

$queryfire = mysqli_query($con, $query);

$num = mysqli_num_rows($queryfire);

while($account = mysqli_fetch_array($queryfire)) {
	if ($account['login'] === $_SESSION['user'])
		break ;
}

if (isset($_POST['password']) && isset($_POST['confirm_password'])) {
	if ($_POST['password'] && $_POST['confirm_password']) {
		if ($_POST['password'] !== $_POST['confirm_password']) {
			echo "<script type='text/javascript'>alert(`Please, confirm your password!`);</script>";
			echo '<script>window.location="register.php"</script>';
			exit();
		}

		$username = $_SESSION['user'];
		$password = $_POST['password'];
		$password = hash("whirlpool", $password);
		$query = "UPDATE `users` SET `password` = '$password' WHERE `users`.`login` = '$username';";

		$queryfire = mysqli_query($con, $query);
		echo "<script type='text/javascript'>alert(`You're successfully changed your password!`);</script>";
		echo '<script>window.location="account.php"</script>';
	}
	else {
		echo "<script type='text/javascript'>alert(`Please complete ALL fields.`);</script>";
		echo '<script>window.location="register.php"</script>';
		exit();
	}
}
?>

<html>
<head>
	<title>My online shop | Account</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="styles.css">
</head>
<body style="font-family: 'Futura', sans-serif;">
<div class="navbar">
	<a href="./">Home</a>
	<a class="active" href="#">My account</a>
	<a href="cart.php">Cart</a>
</div>
<h1 class="header">My online shop</h1>
<h2	class="header">Account</h2>
</div>

<div class="">
	<form method="post">
	<label>Hello, <?php
		echo $_SESSION['user'].'! </br>';
	if ($account['type'] === "admin")
		{
			echo "You're an admin!</br>";
		} ?></label>
	<span class="help-block"></span>
	<input type="submit" name="logout" value="Log Out" />
	</form>
</div>

<form method="post">
<div class="form-group ">
	<h5>Edit your password:</h5>
	<label>New password:</label>
	<input type="password" name="password" class="form-control" value="">
	<span class="help-block"></span>
</div>
<div class="form-group ">
	<label>Confirm new password:</label>
	<input type="password" name="confirm_password" class="form-control" value="">
	<span class="help-block"></span>
</div>
<div>
	<input type="submit" name="submit" value="Submit" style="margin-top: 1rem;">
</div>
</form>

<?php
if ($account['type'] === "admin")
{
	echo "<div style='margin-top: 2rem'>As an admin, you can moderate this site, so here's your admin section:</div>
<div id='block_container'>
<form id='block1' method=\"post\">
<div class=\"form-group \">
<h5>Manage users:</h5>
	<label>Login:</label>
	<input type=\"text\" name=\"login_adm\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Password:</label>
	<input type=\"password\" name=\"password_adm\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Group (user or admin):</label>
	<input type=\"text\" name=\"group_adm\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div>
	<input type=\"submit\" name=\"add_user\" value=\"Add\" style=\"margin-top:1rem;\">
	<input type=\"submit\" name=\"edit_user\" value=\"Edit\" style=\"margin-top:1rem;\">
	<input type=\"submit\" name=\"delete_user\" value=\"Delete\" style=\"margin-top:1rem;\">
</div>
</form>

<form id='block2' method=\"post\">
<div class=\"form-group \">
<h5>Manage products and their categories:</h5>
	<label>Product name:</label>
	<input type=\"text\" min=0 name=\"product_name\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Product categories:</label>
	<input type=\"password\" name=\"password\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Product image:</label>
	<input type=\"text\" name=\"group\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Product price:</label>
	<input type=\"number\" min=0 name=\"group\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Product discount:</label>
	<input type=\"number\" min=0 max=99 name=\"group\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Product rate:</label>
	<input type=\"number\" min=0 max=5 name=\"group\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div>
	<input type=\"submit\" name=\"add\" value=\"Add\" style=\"margin-top:1rem;\">
	<input type=\"submit\" name=\"edit\" value=\"Edit\" style=\"margin-top:1rem;\">
	<input type=\"submit\" name=\"delete\" value=\"Delete\" style=\"margin-top:1rem;\">
</div>
</form>


</div>
<form id='block2' method=\"post\">
<div class=\"form-group \">
<h5>Manage client's orders:</h5>
<div class=\"form-group \">
	<label>Order's id:</label>
	<input type=\"number\" name=\"order_id\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Price:</label>
	<input type=\"text\" name=\"order_price\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Item's id:</label>
	<input type=\"number\" min=0 name=\"order_product_id\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div class=\"form-group \">
	<label>Item's quantity:</label>
	<input type=\"number\" min=0 max=99 name=\"order_product_quantity\" class=\"form-control\" value=\"\">
	<span class=\"help-block\"></span>
</div>
<div>
	<input type=\"submit\" name=\"add_order\" value=\"Add\" style=\"margin-top:1rem;\">
	<input type=\"submit\" name=\"edit_order\" value=\"Edit\" style=\"margin-top:1rem;\">
	<input type=\"submit\" name=\"delete_order\" value=\"Delete\" style=\"margin-top:1rem;\">
</div>
</form>
";
}
	?>
</body>
</html>
