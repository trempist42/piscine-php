<?php
session_start();
$con = mysqli_connect("127.0.0.1", "root", "root", "my_online_shop");
if ($con === false)
	exit("Error: Couldn't connect to DB!".mysqli_connect_error());
mysqli_select_db($con, "my_online_shop");

$query = "SELECT `id`, `name`, `category`, `image`, `price`, `discount`, `rate` FROM `my_online_shop` ORDER BY `id` ASC ";

$queryfire = mysqli_query($con, $query);

$num = mysqli_num_rows($queryfire);

if ($_POST && array_key_exists("add", $_POST))
{
	if ($_POST["add"] === "Add to cart")
	{
		$keys = array_keys($_POST);
		if ($prod_id = preg_grep("/quan_.*/", $keys))
		{
			$prod_id = array_values($prod_id);
			$prod_id = $prod_id[0];
			$prod_id = preg_replace("/quan_.*?/", "", $prod_id);
			if (!$_POST["quan_$prod_id"] || $_POST["quan_$prod_id"] < 0)
				echo "<script type='text/javascript'>alert(`Error: you didn't add anything to your cart!`);</script>";
			else
			{
				echo "<script type='text/javascript'>alert('Added to cart!');</script>";
			}
		}
		else
			echo "<script type='text/javascript'>alert(`Error: you didn't add anything to your cart!`);</script>";
	}
}
?>

<html>
<head>
<title>My online shop</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="styles.css">
</head>
<body style="font-family: 'Futura', sans-serif;">
	<div class="navbar">
		<a class="active" href="#">Home</a>
		<a href="account.php">My account</a>
		<a href="cart.php">Cart</a>
	</div>
	<h1 class="header">My online shop</h1>
	<h2	class="header">Here's what we sell!</h2>
	<div class="row">
	<?php
	if ($num > 0)
	{
		while ($product = mysqli_fetch_array($queryfire))
		{
			?>
			<div class="col">
				<form method="post">
					<div class="card">
						<h4 class="card-title"><?php echo $product["name"]; ?></h4>
						<h6 class="card-category">Categories:<br \><?php echo $product["category"]; ?></h6>
						<div class="card-body">
							<img height="250" src="<?php echo $product["image"] ?>" alt='An img of "<?php echo $product["name"]; ?>"' class="card-image">
							<h6>$<?php echo $product["price"]; ?>
							<span>(<?php echo $product["discount"]; ?>% off)</span></h6>
							<h6>Rating: <?php echo $product["rate"]; ?>★</h6>
							<input type="number" name="quan_<?php echo $product["id"] ?>" placeholder="Quantity" min="0" max="10" value="0">
						</div>
						<div class="">
							<input type="submit" name="add" value="Add to cart" />
						</div>
					</div>
				</form>
			</div>
		<?php
		}
	}
	?>
	</div>
</body>
</html>
