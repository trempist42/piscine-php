#!/usr/bin/php
<?php
do
{
    echo "Enter a number: ";
    $buf = fgets(STDIN);
    $num = trim($buf);
    if (is_numeric($num))
    {
        $tmp = $num[strlen($num) - 1];
        if ($tmp % 2)
            echo "The number $num is odd\n";
        else
            echo "The number $num is even\n";
    }
    elseif ($buf != false)
        echo "'$num' is not a number\n";
}
while ($buf != false);
echo "\n";
?>