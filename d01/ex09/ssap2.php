#!/usr/bin/php
<?php
if ($argc < 2)
	exit();
function my_sort($a, $b)
{
	$i = 0;
	$line = "abcdefghijklmnopqrstuvwxyz0123456789!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
	while (($i < strlen($a)) && ($i < strlen($b)))
	{
		$a_index = stripos($line, $a[$i]);
		$b_index = stripos($line, $b[$i]);
		if ($a_index > $b_index)
			return (1);
		elseif ($a_index < $b_index)
			return (-1);
		else
			$i++;
	}
	if (strlen($a) <= strlen($b))
	    return (0);
	else
	    return (1);
}
$argv[0] = " ";
$str = explode(" ", preg_replace("/ +/", " ", trim(implode(" ", $argv))));
usort($str, "my_sort");
foreach ($str as $elem)
	echo $elem."\n";
?>