#!/usr/bin/php
<?php
if ($argc != 2)
{
	echo "Incorrect Parameters\n";
	exit();
}
$n = sscanf($argv[1], " %d %c %d %s ");
if (!is_numeric($n[0]) || !is_numeric($n[2]) || $n[3] != "")
{
	echo "Syntax Error\n";
	exit();
}
if ($n[1] == "+")
{
	echo $n[0] + $n[2];
	echo "\n";
}
elseif ($n[1] == "-")
{
	echo $n[0] - $n[2];
	echo "\n";
}
elseif ($n[1] == "*")
{
	echo $n[0] * $n[2];
	echo "\n";
}
elseif ($n[1] == "/")
{
	echo $n[0] / $n[2];
	echo "\n";
}
elseif ($n[1] == "%")
{
	echo $n[0] % $n[2];
	echo "\n";
}
else
	echo "Syntax Error\n";
?>