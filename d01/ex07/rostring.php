#!/usr/bin/php
<?php
if ($argc < 2)
	exit();
$str = explode(" ", preg_replace("/ +/", " ", trim($argv[1])));
$str[sizeof($str)] = $str[0];
array_splice($str, 0, 1);
$str = implode(" ", $str);
echo $str."\n";
?>