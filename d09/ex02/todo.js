function restoreCookie()
{
	var list = document.getElementById('ft_list');
	var cook = document.cookie;
	cook = cook.split('=')[1].replace(new RegExp('-', 'g'), '=');
	list.innerHTML = atob(cook);
}

function storeCookie()
{
	var list = document.getElementById('ft_list');
	document.cookie = "l=" + btoa(list.innerHTML).replace(new RegExp('=', 'g'), '-');
}

function delTodo(toDo) {
	var isConfirmed = confirm('Are you sure you want to remove this TO DO?');
	if (isConfirmed === true)
	{
		toDo.remove();
		storeCookie();
	}
}

function myPrompt()
{
	var toDo = prompt('Type your new TO DO:', '');
	if (toDo !== null && toDo !== '')
	{
		var newElement = document.createElement('div');
		var_text = document.createTextNode(toDo);
		newElement.appendChild(var_text);
		newElement.setAttribute('onclick', "delTodo(this);");
		var list = document.getElementById('ft_list');
		list.insertBefore(newElement, list.childNodes[0]);
		storeCookie();
	}
}
