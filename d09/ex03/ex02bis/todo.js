function restoreCookie()
{
	var $list = (($("#ft_list").toArray())[0]);
	var $cook = document.cookie;
	$cook = $cook.split('=')[1].replace(new RegExp('-', 'g'), '=');
	$list.innerHTML = atob($cook);

}

function storeCookie()
{
	var $list = (($("#ft_list").toArray())[0]).innerHTML;
	document.cookie = "l=" + btoa($list).replace(new RegExp('=', 'g'), '-');
}

function delTodo($toDo) {
	var $isConfirmed = confirm('Are you sure you want to remove this TO DO?');
	if ($isConfirmed === true)
	{
		$toDo.remove();
		storeCookie();
	}
}

function myPrompt()
{
	var $toDo = prompt('Type your new TO DO:', '');
	if ($toDo !== null && $toDo !== '')
	{
		var $list = $("#ft_list");
		$list.prepend("<div onclick='delTodo($(this));'>" + $toDo + "</div>");
		storeCookie();
	}
}
