SELECT `film`.`id_genre`, `genre`.`name` AS `name_genre`, `distrib`.`id_distrib`, `distrib`.`name` AS `name_distrib`, `film`.`title` AS `title_film`
FROM `db_rafalmer`.`film` LEFT OUTER JOIN `db_rafalmer`.`genre` ON `db_rafalmer`.`genre`.`id_genre` = `db_rafalmer`.`film`.`id_genre` LEFT OUTER JOIN `db_rafalmer`.`distrib` ON `db_rafalmer`.`distrib`.`id_distrib` = film.`id_distrib`
WHERE `film`.`id_genre` >= 4 AND `film`.`id_genre` <= 8;
