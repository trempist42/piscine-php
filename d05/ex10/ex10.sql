SELECT `title` AS `Title`, `summary`AS `Summary`, `prod_year`
FROM `db_rafalmer`.`film` INNER JOIN `db_rafalmer`.`genre` ON `genre`.`id_genre` = `film`.`id_genre`
WHERE `name` = 'erotic'
ORDER BY `prod_year` DESC;
