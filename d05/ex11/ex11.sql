SELECT UPPER(`last_name`) AS `NAME`, `first_name`, `price`
FROM `db_rafalmer`.`user_card` INNER JOIN `db_rafalmer`.`member` ON `db_rafalmer`.`member`.`id_user_card` = `db_rafalmer`.`user_card`.`id_user` INNER JOIN `db_rafalmer`.`subscription` ON `db_rafalmer`.`subscription`.`id_sub` = `db_rafalmer`.`member`.`id_sub`
WHERE `price` > 42
ORDER BY `last_name` ASC, `first_name` ASC ;
