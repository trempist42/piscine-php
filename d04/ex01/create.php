<?php
if ($_POST)
{
	if ($_POST["login"] === "" || $_POST["passwd"] === "" || $_POST["submit"] !== "OK")
	{
		echo "ERROR\n";
		return ;
	}
	if (!file_exists("../private"))
		mkdir ("../private");
	if (file_exists("../private/passwd"))
	{
		$u_passwd = unserialize(file_get_contents("../private/passwd"));
		foreach ($u_passwd as $user)
		{
			if ($user["login"] === $_POST["login"])
			{
				echo "ERROR\n";
				return ;
			}
		}
	}
	$tab["login"] = $_POST["login"];
	$tab["passwd"] = hash('whirlpool', $_POST["passwd"]);
	$u_passwd[] = $tab;
	file_put_contents("../private/passwd", serialize($u_passwd));
	echo "OK\n";
}
?>