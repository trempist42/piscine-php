<?php

Class Vector
{
	static $verbose = false;

	private $_x;
	private $_y;
	private $_z;
	private $_w;

	static function doc()
	{
		return PHP_EOL . file_get_contents('Vector.doc.txt');
	}

	function __construct(array $kwargs)
	{
		if (isset($kwargs['dest']))
		{
			if (!isset($kwargs['orig']))
				$kwargs['orig'] = new Vertex(array('x' => 0, 'y' => 0, 'z' => 0));
			$this->_x = $kwargs['dest']->getX() - $kwargs['orig']->getX();
			$this->_y = $kwargs['dest']->getY() - $kwargs['orig']->getY();
			$this->_z = $kwargs['dest']->getZ() - $kwargs['orig']->getZ();
			$this->_w = 0;
		}
		if (self::$verbose === true)
			print ((string)$this . ' constructed' . PHP_EOL);
		return;
	}

	function __destruct()
	{
		if (self::$verbose === true)
			print ((string)$this . ' destructed' . PHP_EOL);
		return;
	}

	function __toString()
	{
		return vsprintf("Vector( x:%.2f, y:%.2f, z:%.2f, w:%.2f )", array($this->_x, $this->_y, $this->_z, $this->_w));
	}

	function getX()
	{
		return (float)$this->_x;
	}

	function getY()
	{
		return (float)$this->_y;
	}

	function getZ()
	{
		return (float)$this->_z;
	}

	function getW()
	{
		return (float)$this->_w;
	}

	function magnitude()
	{
		return (float)sqrt(pow($this->_x, 2) + pow($this->_y, 2) + pow($this->_z, 2));
	}

	function normalize()
	{
		$length = $this->magnitude();
		$inv_length = (1 / $length);
		return new Vector(array(
			'dest' => new Vertex(array(
				'x' => $this->_x * $inv_length,
				'y' => $this->_y * $inv_length,
				'z' => $this->_z * $inv_length
		))));
	}

	function add(Vector $rhs)
	{
		return new Vector(array(
			'dest' => new Vertex(array(
				'x' => $this->_x + $rhs->_x,
				'y' => $this->_y + $rhs->_y,
				'z' => $this->_z + $rhs->_z
		))));
	}

	function sub(Vector $rhs)
	{
		return new Vector(array(
			'dest' => new Vertex(array(
				'x' => $this->_x - $rhs->_x,
				'y' => $this->_y - $rhs->_y,
				'z' => $this->_z - $rhs->_z
		))));
	}

	function opposite()
	{
		return new Vector(array(
			'dest' => new Vertex(array(
				'x' => $this->_x * -1,
				'y' => $this->_y * -1,
				'z' => $this->_z * -1
		))));
	}

	function scalarProduct($k)
	{
		return new Vector(array(
			'dest' => new Vertex(array(
				'x' => $this->_x * $k,
				'y' => $this->_y * $k,
				'z' => $this->_z * $k
		))));
	}

	function dotProduct(Vector $rhs)
	{
		return (float)(($this->_x * $rhs->_x) + ($this->_y * $rhs->_y) + ($this->_z * $rhs->_z));
	}

	function cos(Vector $rhs)
	{
		return (float)($this->dotProduct($rhs) /
			sqrt((pow($this->_x, 2) + pow($this->_y, 2) + pow($this->_z, 2)) *
					(pow($rhs->_x, 2) + pow($rhs->_y, 2) + pow($rhs->_z, 2))));
	}

	function crossProduct(Vector $rhs)
	{
		return new Vector(array(
			'dest' => new Vertex(array(
				'x' => ($this->_y * $rhs->getZ()) - ($this->_z * $rhs->getY()),
				'y' => ($this->_z * $rhs->getX()) - ($this->_x * $rhs->getZ()),
				'z' => ($this->_x * $rhs->getY()) - ($this->_y * $rhs->getX())
			))));
	}
}

?>