<?php

Class Color
{
	static $verbose = false;

	public $red;
	public $green;
	public $blue;

	static function doc()
	{
		return PHP_EOL . file_get_contents('Color.doc.txt');
	}

	function __construct(array $kwargs)
	{
		if (isset($kwargs['red']) && isset($kwargs['green']) && isset($kwargs['blue']))
		{
			$this->red = (int)$kwargs['red'];
			$this->green = (int)$kwargs['green'];
			$this->blue = (int)$kwargs['blue'];
		}
		elseif (isset($kwargs['rgb']))
		{
			$this->red = $kwargs['rgb'] >> 16;
			$this->green = ($kwargs['rgb'] >> 8) - ($this->red << 8);
			$this->blue = $kwargs['rgb'] - ($this->red << 16) - ($this->green << 8);
		}
		if (self::$verbose === true)
			print ((string)$this . ' constructed.' . PHP_EOL);
		return;
	}

	function __destruct()
	{
		if (self::$verbose === true)
			print ((string)$this . ' destructed.' . PHP_EOL);
		return;
	}

	function __toString()
	{
		return vsprintf("Color( red: %3d, green: %3d, blue: %3d )", array($this->red, $this->green, $this->blue));
	}

	function add($color)
	{
		return new Color(array('red' => $this->red + $color->red, 'green' => $this->green + $color->green, 'blue' => $this->blue + $color->blue));
	}

	function sub($color)
	{
		return new Color(array('red' => $this->red - $color->red, 'green' => $this->green - $color->green, 'blue' => $this->blue - $color->blue));
	}

	function mult($multiplier)
	{
		return new Color(array('red' => $this->red * $multiplier, 'green' => $this->green * $multiplier, 'blue' => $this->blue * $multiplier));
	}
}
?>