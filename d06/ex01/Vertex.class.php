<?php

require_once 'Color.class.php';

Class Vertex
{
	static $verbose = false;

	private $_x;
	private $_y;
	private $_z;
	private $_w;
	private $_color;

	static function doc()
	{
		return PHP_EOL . file_get_contents('Vertex.doc.txt');
	}

	function __construct(array $kwargs)
	{
		if (isset($kwargs['x']) && isset($kwargs['y']) && isset($kwargs['z']))
		{
			if (isset($kwargs['color']))
				$this->_color = $kwargs['color'];
			else
				$this->_color = new Color(array('red' => 255, 'green' => 255, 'blue' => 255));
			if (isset($kwargs['w']))
				$this->_w = $kwargs['w'];
			else
				$this->_w = 1.0;
			$this->_x = $kwargs['x'];
			$this->_y = $kwargs['y'];
			$this->_z = $kwargs['z'];
		}
		if (self::$verbose === true)
			print (vsprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f, ", array($this->_x, $this->_y, $this->_z, $this->_w)).(string)$this->_color.' )' . ' constructed' . PHP_EOL);
		return;
	}

	function __destruct()
	{
		if (self::$verbose === true)
			print (vsprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f, ", array($this->_x, $this->_y, $this->_z, $this->_w)).(string)$this->_color.' )' . ' destructed' . PHP_EOL);
		return;
	}

	function __toString()
	{
		if (self::$verbose === false)
			return vsprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f )", array($this->_x, $this->_y, $this->_z, $this->_w));
		else
			return vsprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f, ", array($this->_x, $this->_y, $this->_z, $this->_w)).(string)$this->_color.' )';
	}

	function getX()
	{
		return (float)$this->_x;
	}

	function getY()
	{
		return (float)$this->_y;
	}

	function getZ()
	{
		return (float)$this->_z;
	}

	function getW()
	{
		return (float)$this->_w;
	}

	function getColor()
	{
		return $this->_color;
	}

	function setX($x)
	{
		$this->_x = $x;
	}

	function setY($y)
	{
		$this->_y = $y;
	}

	function setZ($z)
	{
		$this->_z = $z;
	}

	function setW($w)
	{
		$this->_w = $w;
	}

	function setColor($color)
	{
		$this->_color = $color;
	}
}

?>