<- Vertex ----------------------------------------------------------------------
The Vertex class handles 3D vertexes.

An instance can be contructed from the x, y, z coordinates:
new Vertex( array( 'x' => 1.0, 'y' => 0.0, 'z' => 0.0 ) );

Optionally, you can add the 'w' (for the homogenous parameter) and 'color'
(for the color):
new Vertex( array( 'x' => 1.0, 'y' => 0.0, 'z' => 0.0 ) );

Their default values are as follows:
'w' => 1.0
'color' => new Color(array('red' => 255, 'green' => 255, 'blue' => 255))

The class provides the following methods :

float	getX();
Gets x coordinate from the current instance and returns it.

float	getY();
Gets y coordinate from the current instance and returns it.

float	getZ();
Gets z coordinate from the current instance and returns it.

float	getW();
Gets the homogenous parameter from the current instance and returns it.

Color	getColor();
Gets the color from the current instance and returns it.

void	setX( $x );
Sets x coordinate for the current instance.

void	setY( $y );
Sets y coordinate for the current instance.

void	setZ( $z );
Sets z coordinate for the current instance.

void	setW( $w );
Sets the homogenous parameter coordinate for the current instance.

void	setColor( Color $color );
Sets the color for the current instance.
---------------------------------------------------------------------- Vertex ->
