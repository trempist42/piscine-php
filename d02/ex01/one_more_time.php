#!/usr/bin/php
<?php
if ($argc < 2)
	exit();
$date = explode(" ", $argv[1]);
if (sizeof($date) != 5 || !preg_match("/^[0-9]{4}$/", $date[3]) || !preg_match("/^[0-9]|[0-3][0-9]$/", $date[1]) || !preg_match("/^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$/", $date[4]))
{
	echo "Wrong Format\n";
	exit();
}
$time = explode(":", $date[4]);
if (!preg_match("/^([0-1][0-9])|[2][0-3]$/", $time[0]) || !preg_match("/^[0-5][0-9]$/", $time[1]) || !preg_match("/^[0-5][0-9]$/", $time[2]))
{
	echo "Wrong Format\n";
	exit();
}
$week_day = $date[0];
if ($week_day  ===  "Lundi" ||  $week_day  ===  "lundi")
	$week_day = 1;
elseif ($week_day  ===  "Mardi" || $week_day  ===  "mardi")
	$week_day = 2;
elseif ($week_day  ===  "Mercredi" || $week_day  ===  "mercredi")
	$week_day = 3;
elseif ($week_day  ===  "Jeudi" || $week_day  ===  "jeudi")
	$week_day = 4;
elseif ($week_day  ===  "Vendredi" || $week_day  ===  "vendredi")
	$week_day = 5;
elseif ($week_day  ===  "Samedi" || $week_day  ===  "samedi")
	$week_day = 6;
elseif ($week_day  ===  "Dimanche" || $week_day  ===  "dimanche")
	$week_day = 7;
else
{
	echo "Wrong Format\n";
	exit();
}
$day = $date[1];
if ($day > 31)
{
	echo "Wrong Format\n";
	exit();
}
$month = $date[2];
if ($month === "janvier" || $month === "Janvier")
	$month = 1;
elseif ($month === "fevrier" || $month === "Fevrier" || $month === "février" || $month === "Février")
	$month = 2;
elseif ($month === "mars" || $month === "Mars")
	$month = 3;
elseif ($month === "avril" || $month === "Avril")
	$month = 4;
elseif ($month === "mai" || $month === "Mai")
	$month = 5;
elseif ($month === "juin" || $month === "Juin")
	$month = 6;
elseif ($month === "juillet" || $month === "Juillet")
	$month = 7;
elseif ($month === "aout"|| $month === "Aout" || $month === "août" || $month === "Août")
	$month = 8;
elseif ($month === "septembre" || $month === "Septembre")
	$month = 9;
elseif ($month === "octobre" || $month === "Octobre")
	$month = 10;
elseif ($month === "novembre"  || $month === "Novembre")
	$month = 11;
elseif ($month === "decembre"  || $month === "Decembre" || $month === "décembre"  || $month === "Décembre")
	$month = 12;
else
{
	echo "Wrong Format\n";
	exit();
}
$year = $date[3];
$hours = $time[0];
$minutes = $time[1];
$seconds = $time[2];
date_default_timezone_set("Europe/Paris");
$unix_time = mktime($hours, $minutes, $seconds, $month, $day, $year);
$tmp = date('N', $unix_time);
if (intval($tmp) !== $week_day)
{
	echo "Wrong Format\n";
	exit();
}
echo "$unix_time\n";
?>