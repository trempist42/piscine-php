#!/usr/bin/php
<?php
if ($argc < 2)
	exit();
$line = file_get_contents($argv[1]);
$line = preg_replace_callback('/<a.{0,}?title="(.{0,}?)">/',
	function ($matches)
	{
		return (str_replace($matches[1], strtoupper($matches[1]), $matches[0]));
	},
	$line);
$line = preg_replace_callback('/<a.{0,}?>(.{0,}?)</',
	function ($matches)
	{
		return (str_replace($matches[1], strtoupper($matches[1]), $matches[0]));
	},
	$line);
echo $line;
?>