#!/usr/bin/php
<?php
$file = fopen("/var/run/utmpx", "r");
date_default_timezone_set("Europe/Moscow");
$i = 0;
$user[0] = "";
while (!feof($file))
{
	if ($str = fread($file, 628))
	{
		$data = unpack('a256user/a4id/a32line/i1pid/i1type/i1time', $str);
		if ($data['type'] == 7)
		{
			$user[$i] = trim($data['user']);
			$line[$i] = trim($data['line']);
			$time_int[$i] = $data['time'];
			$time[$i] = date("M  j H:i ", $time_int[$i]);
			$i++;
		}
	}
}
if ($user[0] !== "")
{
	array_multisort($time_int, $user, $line, $time);
	for ($j = 0; $j < $i; $j++)
		echo "$user[$j] $line[$j]  $time[$j]\n";
}
?>